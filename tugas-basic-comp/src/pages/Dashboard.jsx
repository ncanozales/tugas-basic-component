import React, {useEffect} from 'react'

import { Link } from 'react-router-dom'

import Chart from 'react-apexcharts'

import { useSelector } from 'react-redux'

import StatusCard from '../components/status-card/StatusCard'

import Table from '../components/table/Table'

import Badge from '../components/badge/Badge'

import statusCards from '../assets/JsonData/status-card-data.json'
import { useState } from 'react'
import Modal from './Modal'
import Modal2 from './Modal2'
import Modal3 from './Modal3'
import '../components/topnav/topnav.css'


const Dashboard = () => {

    const chartOptions = {
        series: [{
            name: 'Online Customers',
            data: [40,70,20,90,36,80,30,91,60]
        }, {
            name: 'Store Customers',
            data: [40, 30, 70, 80, 40, 16, 40, 20, 51, 10]
        }],
        options: {
            color: ['#6ab04c', '#2980b9'],
            chart: {
                background: 'transparent'
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep']
            },
            legend: {
                position: 'top'
            },
            grid: {
                show: false
            }
        }
    }
    
    const topCustomers = {
        head: [
            'user',
            'total orders',
            'total sMenunggu'
        ],
        body: [
            {
                "username": "john doe",
                "order": "490",
                "price": "$15,870"
            },
            {
                "username": "frank iva",
                "order": "250",
                "price": "$12,251"
            },
            {
                "username": "anthony baker",
                "order": "120",
                "price": "$10,840"
            },
            {
                "username": "frank iva",
                "order": "110",
                "price": "$9,251"
            },
            {
                "username": "anthony baker",
                "order": "80",
                "price": "$8,840"
            }
        ]
    }
    
    const renderCusomerHead = (item, index) => (
        <th key={index}>{item}</th>
    )
    
    const renderCusomerBody = (item, index) => (
        <tr key={index}>
            <td>{item.username}</td>
            <td>{item.order}</td>
            <td>{item.price}</td>
        </tr>
    )
    
    const latestOrders = {
        header: [
            "ID Produk",
            "Nama Produk",
            "Total Harga",
            "Tanggal Produksi",
            "Delete"
        ],
        body: [
            {
                id: "#TEFA01",
                user: "Tupperware",
                date: "20 Jun 2021",
                price: "Rp 7.500.000",
                status: "Sampai"
            },
            {
                id: "#TEFA02",
                user: "Ideapad Gaming",
                date: "21 Jun 2021",
                price: "Rp 15.000.000",
                status: "Terkirim"
            },
            {
                id: "#TEFA03",
                user: "Nasi Goreng",
                date: "22 Jun 2021",
                price: "Rp 1.000.000",
                status: "Menunggu"
            },
            {
                id: "#TEFA04",
                user: "Sepatu Yonex",
                date: "23 Jun 2021",
                price: "Rp 12.500.000",
                status: "Terkirim"
            },
            {
                id: "#TEFA05",
                user: "Samsung Axiata",
                date: "24 Jun 2021",
                price: "Rp 10.000.000",
                status: "Dihapus"
            }
        ]
    }
    
    const orderStatus = {
        "Sampai": "primary",
        "Menunggu": "warning",
        "Terkirim": "success",
        "Dihapus": "danger"
    }
    
    const renderOrderHead = (item, index) => (
        <th key={index}>{item}</th>
    )

    const renderOrderBody = (item, index) => (

        <tr  key={index}>
            <td  onClick={setOpenModal2.bind(true)}>{item.id}</td>
            <td  onClick={setOpenModal2.bind(true)}>{item.user}</td>
            <td  onClick={setOpenModal2.bind(true)}>{item.price}</td>
            <td  onClick={setOpenModal2.bind(true)}>{item.date}</td>
            <td>
                <button onClick={setOpenModal.bind(true)}  style={{borderRadius: "10px",padding: "5px", backgroundColor: "white"}}>
                    <i  style={{ fontSize: "40px", color: "#349eff" }} className='bx bx-trash'></i>
                </button>
                
                {/* <Badge type={orderStatus[item.status]} content={item.status}/> */}
            </td>
        </tr>
    )


    const themeReducer = useSelector(state => state.ThemeReducer.mode)
    const [openModal, setOpenModal] = useState(false)
    const [openModal2, setOpenModal2] = useState(false)
    const [openModal3, setOpenModal3] = useState(false)
    const [status, setStatus] = useState("Barang INI")

    return (
        <div>
            <Modal  status={status} open={openModal} onClose={() => setOpenModal(false)}/>
            <Modal2  status={status} open={openModal2} onClose={() => setOpenModal2(false)}/>
            <Modal3  status={status} open={openModal3} onClose={() => setOpenModal3(false)}/>
            <h2 className="page-header">Daftar Products</h2>

                <div className="col-12">
                    <div className="card">
                        <div className="card__header">

                            {/* <button className="notification-item" onClick={setOpenModal3.bind(true)}>
                                <i className='bx bx-plus'></i>
                                <span>Tambah Produk</span>
                            </button> */}
                            <Link 
                            className="notification-item"
                            role="button"
                            to="/products/new"
                            > 
                            <i className='bx bx-plus'></i>
                            <span>Tambah Produk</span>
                            </Link>

                        </div>
                        <div className="card__body">
                            <Table
                                headData={latestOrders.header}
                                renderHead={(item, index) => renderOrderHead(item, index)}
                                bodyData={latestOrders.body}
                                renderBody={(item, index) => renderOrderBody(item, index)}
                            />
                        </div>

                    </div>
                </div>
            </div>
    )
}

export default Dashboard
