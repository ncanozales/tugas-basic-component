import React from "react";
import gambar from '../assets/images/3.png'

const Modal = ({open, status, onClose}) => {
    if (!open) return null
    return(
        <div className="overlay">
            <div className="modalContainer"> 
                <img src={gambar} alt="" />
                <div className="modalRight">
                    <div className="content">
                        <p>Tupperware ini Dimiliki oleh</p>
                        <h1>Niclos Canozales</h1>
                        <p>#TEFA01</p>
                    </div>
                    <div className="btnContainer">

                        <button  onClick={onClose} className="btnOutline">
                            <span className="bold">Dimengerti</span>
                        </button>
                    </div>
                </div>
            </div>
        </div> 
    )
}

export default Modal