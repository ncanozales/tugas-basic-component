import React from "react";
import gambar from '../assets/images/62.png'

const Modal = ({open, status, onClose}) => {
    if (!open) return null
    return(

            <div className="modalContainer"> 
                <img src={gambar} alt="" />
                <div className="modalRight">
                    <div className="content">
                        <p style={{marginBottom: 18, fontSize: 20}}>Tambahkan Produk</p>

                        <div className="wrapper">
                            <div class="input-data">
                                <input type="text" required />
                                <div class="underline"></div>
                                <label>Nama Produk</label>
                            </div>
                        </div>

                        <div className="wrapper">
                            <div class="input-data">
                                <input type="text" required />
                                <div class="underline"></div>
                                <label>Harga Produk</label>
                            </div>
                        </div>

                        <div className="wrapper">
                            <div class="input-data">
                                <input type="text" required />
                                <div class="underline"></div>
                                <label>Tanggal Produksi</label>
                            </div>
                        </div>

                    </div>
                    <div className="btnContainer">

                        <button  onClick={onClose} className="btnOutline">
                            <span className="bold">Batalkan</span>
                        </button>

                        <button onClick={onClose} className="btnPrimary">
                            <span className="bold">Tambah Produk</span>
                        </button>
                    </div>
                </div>
            </div>

    )
}

export default Modal