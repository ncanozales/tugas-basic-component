import React from "react";
import gambar from '../assets/images/10.png'

const Modal = ({open, status, onClose}) => {
    if (!open) return null
    return(
        <div className="overlay">
            <div className="modalContainer"> 
                <img src={gambar} alt="" />
                <div className="modalRight">
                    <div className="content">
                        <p>Ingin menghapus</p>
                        <h1>{status}</h1>
                        <p>Dari Daftar Products ?</p>
                    </div>
                    <div className="btnContainer">
                    <button  onClick={onClose} className="btnOutline">
                            <span className="bold">Batalkan</span>
                        </button>
                        <button onClick={onClose} className="btnPrimary">
                            <span className="bold">Silahkan</span>
                        </button>

                    </div>
                </div>
            </div>
        </div> 
    )
}

export default Modal