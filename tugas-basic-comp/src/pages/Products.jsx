import React from 'react'
import gambar from '../assets/images/62.png'
import { Link } from 'react-router-dom'
import './produk.js'
import {useEffect} from 'react';

const Products = () => {

    useEffect(() => {
        const selected = document.querySelector('.selected');
const optionsContainer = document.querySelector('.options-container');

const optionsList = document.querySelectorAll('.option');

selected.addEventListener('click', () => {
  optionsContainer.classList.toggle('active');
});

optionsList.forEach((o) => {
  o.addEventListener('click', () => {
    selected.innerHTML = o.querySelector('label').innerHTML;
    optionsContainer.classList.remove('active');
  });
});

const form = document.querySelector("form"),
fileInput = document.querySelector(".file-input"),
progressArea = document.querySelector(".progress-area"),
uploadedArea = document.querySelector(".uploaded-area");

// form click event
form.addEventListener("click", () =>{
  fileInput.click();
});


    }, []);
    return (
        <div className="overlay">
            <div className="modalContainer2"> 

                <div className="modalRight">
                    <div className="content">
                        <p style={{marginBottom: 18, fontSize: 20}}>Tambahkan Produk</p>

                        <div className="wrapper">
                            <div class="input-data">
                                <input type="text" required />
                                <div class="underline"></div>
                                <label>Nama Produk</label>
                            </div>
                        </div>

                        <div className="wrapper">
                            <div class="input-data">
                                <input type="text" required />
                                <div class="underline"></div>
                                <label>Harga Produk</label>
                            </div>
                        </div>

                        <div className="wrapper">
                            <div class="input-data">
                                <input type="text" required />
                                <div class="underline"></div>
                                <label>Tanggal Produksi</label>
                            </div>
                        </div>


<div className="select-box">
  <div className="options-container">
    <div className="option">
      <input
        type="radio"
        className="radio"
        id="automobiles"
        name="category"
      />
      <label for="automobiles">Elektronik</label>
    </div>

    <div className="option">
      <input type="radio" className="radio" id="film" name="category" />
      <label for="film">Kendaraan</label>
    </div>

    <div className="option">
      <input type="radio" className="radio" id="science" name="category" />
      <label for="science">Furniture</label>
    </div>

    <div className="option">
      <input type="radio" className="radio" id="art" name="category" />
      <label for="art">Olahraga</label>
    </div>

    <div className="option">
      <input type="radio" className="radio" id="music" name="category" />
      <label for="music">Pakaian</label>
    </div>

    <div className="option">
      <input type="radio" className="radio" id="travel" name="category" />
      <label for="travel">Kebutuhan</label>
    </div>


  </div>


  <div className="selected">
    Tentukan Kategori Produk
  </div>





                    </div>
                    <div class="bungkus">
    <header>Silahkan Unggah Gambar Produk</header>
    <form action="#">
      <input class="file-input" type="file" name="file" hidden />
      <i class="fas fa-cloud-upload-alt"></i>
      <p>Tekan untuk Upload</p>
    </form>
  </div>



</div>










                    <div className="btnContainer">
                        
                            <Link 
                            className="btnOutline"
                            role="button"
                            to="/"
                            id="linked"
                            > 
                            <span>Batalkan</span>
                            </Link>


                        <button className="btnPrimary">
                            <span >Tambah Produk</span>
                        </button>
                    </div>
                </div>
            </div>
        </div> 
    )
}

export default Products
